/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appobjetos;

import pruebaobjetos.Punto;

/**
 *
 * @author nachorod
 */
public class AppObjetos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Punto p1=new Punto("p1");
        Punto p2=new Punto("p2",1,2);
        Punto p3=new Punto("p3",5);
        p1.mostrar();
        p2.mostrar();
        p3.mostrar();
        p1.setX(3);
        p1.setY(3);
        p1.mostrar();
        System.out.println("Distancia origen p1: " + p1.distanciaOrigen());
        //probando clonado de nuevo
    }
    
}
