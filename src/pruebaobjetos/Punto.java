/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pruebaobjetos;

/**
 *
 * @author nachorod
 */
public class Punto {
    private double x;
    private double y;
    private String nombre;
    
    public Punto(String nombre){
        this.nombre=nombre;
    }
    
    public Punto(String nombre, double x, double y) {
        this.nombre=nombre;
        this.x=x;
        this.y=y;
    }
    
    public Punto(String nombre, double x) {
        this.nombre=nombre;
        this.x=x;
        this.y=x;
    }
    
    public double getX() {
        return this.x;
    }
    
    public void setX(double x) {
        this.x=x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
    
    public double distanciaOrigen() {
        return redondear(Math.sqrt(x*x+y*y),2);
    }
    
    public double redondear(double num, int decimales) {
        double numDividir=Math.pow(10, decimales);
        double n=num*numDividir;
        n=Math.round(n);
        return n/numDividir;
    }
    
    public void mostrar() {
        System.out.println(nombre +": "+ this.x + " , "+ this.y);
    }
}
